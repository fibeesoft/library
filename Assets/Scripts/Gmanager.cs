﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gmanager : MonoBehaviour {

    [SerializeField] Image img_image;
    [SerializeField] Text txt_text, txt_author, txt_title;
    [SerializeField] BookPage[] bookPage;
    [SerializeField] Button btn_nextPage, btn_prevPage, btn_playAudio, btn_mainMenu;
    [SerializeField] AudioSource audiosrc;
    [SerializeField] GameObject go_mainMenuContainer, go_lastPageContainer, go_levelMenu;

    int pageNumber;
    AudioClip audClip;


    int bookNumber;
    [SerializeField] NewBook[] newBook;


    private void Start()
    {
        CloseLevelMenu();
        HideLastPageContainer();
        pageNumber = 0;
        bookNumber = 0;
        OpenMainMenu();
    }

    //void GetBookPage()
    //{
    //    HideLastPageContainer();
    //    ShowLastPageContainer();
    //    img_image.sprite = bookPage[pageNumber].GetSprite();
    //    txt_text.text = bookPage[pageNumber].GetText();
    //    audClip = bookPage[pageNumber].GetClip();
    //}

    void GetBookPage()
    {
        HideLastPageContainer();
        ShowLastPageContainer();
        img_image.sprite = newBook[bookNumber].GetSprite(pageNumber);
        txt_text.text = newBook[bookNumber].GetText(pageNumber);
        audClip = newBook[bookNumber].GetClip(pageNumber);
    }

    public void NextPage()
    {
        if(pageNumber < bookPage.Length -1)
        {
            pageNumber++;
            GetBookPage();
        }
    }

    public void PrevPage()
    {
        if(pageNumber > 0)
        {
            pageNumber--;
            GetBookPage();
        }
    }

    public void PlayClip()
    {
        if(audClip != null)
        {
            audiosrc.PlayOneShot(audClip);
        }
        else
        {
            print("no audio clip available");
        }

    }

    public void OpenMainMenu()
    {
        go_mainMenuContainer.SetActive(true);
        pageNumber = 0;
        HideLastPageContainer();
    }

    public void CloseMainMenu()
    {
        go_mainMenuContainer.SetActive(false);
        GetBookPage();
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    //public void ShowLastPageContainer()
    //{
    //    if (pageNumber == bookPage.Length - 1)
    //    {
    //        go_lastPageContainer.SetActive(true);
    //        img_image.sprite = bookPage[pageNumber].GetSprite();
    //        txt_text.text = bookPage[pageNumber].GetText();
    //        txt_author.text = "Written by \n" + bookPage[pageNumber].GetAuthor();
    //        txt_title.text = bookPage[pageNumber].GetTitle();
    //    }
    //}

    public void ShowLastPageContainer()
    {
        if (pageNumber == bookPage.Length - 1)
        {
            go_lastPageContainer.SetActive(true);
            txt_author.text = "Written by \n" + newBook[bookNumber].GetAuthor();
            txt_title.text = newBook[bookNumber].GetTitle();
        }
    }


    public void HideLastPageContainer()
    {
        go_lastPageContainer.SetActive(false);
    }

    public void LoadLevelMenu()
    {
        go_levelMenu.SetActive(true);
    }

    public void CloseLevelMenu()
    {
        go_levelMenu.SetActive(false);

    }
}
