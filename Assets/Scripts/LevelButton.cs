﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelButton : MonoBehaviour {

    Gmanager gmanager;

    private void Start()
    {
        gmanager = FindObjectOfType<Gmanager>();
    }

    public void CloseLevelMenu()
    {
        gmanager.CloseLevelMenu();
    }
}
