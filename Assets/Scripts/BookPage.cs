﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Book page")]

public class BookPage : ScriptableObject {

    [TextArea(2, 10)] [SerializeField] string text;
    [SerializeField] Sprite image;
    [SerializeField] AudioClip pageAudioCLip;
    [SerializeField] string txt_author;
    [SerializeField] string title;

    public string GetText()
    {
        return text;
    }

    public Sprite GetSprite()
    {
        return image;
    }

    public AudioClip GetClip()
    {
        return pageAudioCLip;
    }

    public string GetAuthor()
    {
        return txt_author;
    }

    public string GetTitle()
    {
        return title;
    }
}
