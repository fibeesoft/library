﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Book")]

public class NewBook : ScriptableObject
{

    [TextArea(2, 10)] [SerializeField] public string[] text;
    [SerializeField] public Sprite[] image;
    [SerializeField] public AudioClip[] pageAudioCLip;
    [SerializeField] string txt_author;
    [SerializeField] string title;

    //public string[] GetText()
    //{
    //    return text;
    //}

    public string GetText(int textNumber)
    {
        return text[textNumber];
    }

    public Sprite GetSprite(int spriteNumber)
    {
        return image[spriteNumber];
    }

    public AudioClip GetClip(int clipNumber)
    {
        return pageAudioCLip[clipNumber];
    }

    public string GetAuthor()
    {
        return txt_author;
    }

    public string GetTitle()
    {
        return title;
    }
}
